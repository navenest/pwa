import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { registerSW } from 'virtual:pwa-register'
import '@mdi/font/css/materialdesignicons.css'

const intervalMS = 60 * 1000


// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'


const vuetify = createVuetify({
    components,
    directives,
    theme: {
        defaultTheme: 'dark'
    },
    icons: {
        defaultSet: 'mdi', // This is already the default value - only for display purposes
    }
})



registerSW({
    immediate: true,
    onRegisteredSW(swUrl, r) {
        console.log(`Service Worker at: ${swUrl}`)
        r && setInterval(async () => {
            console.log('Checking for sw update')
            await r.update()
        }, intervalMS /* 20s for testing purposes */)
    },
})

createApp(App).use(vuetify).mount('#app')
