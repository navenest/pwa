package main

import (
	"context"
	"errors"
	"gitlab.com/navenest/golang-library/pkg/logger/log"
	"gitlab.com/navenest/golang-library/pkg/opentelemetry"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"pwa/web"
	"time"
)

func main() {
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	// Set up OpenTelemetry.
	otelShutdown, err := opentelemetry.SetupOTelSDK(ctx, nil)
	if err != nil {
		return
	}
	// Handle shutdown properly so nothing leaks.
	defer func() {
		err = errors.Join(err, otelShutdown(context.Background()))
	}()
	webserver := web.App{}
	webserver.Initialize()
	go webserver.Run(":8080")

	<-ctx.Done()

	stop()
	log.Logger.Info("Shutting down ...")

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	go func() {
		if err := webserver.Srv.Shutdown(ctx); err != nil {
			log.Logger.Fatal("Unable to shutdown http server", zap.Error(err))
		}
	}()

	select {
	case <-timeoutCtx.Done():
		if errors.Is(timeoutCtx.Err(), context.DeadlineExceeded) {
			log.Logger.Fatal("timeout exceeded, forcing shutdown")
		}

		os.Exit(0)
	}
}
