package web

import (
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"gitlab.com/navenest/golang-library/pkg/logger/log"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"math/rand"
	"slices"
	"strconv"
	"strings"
	"time"

	"go.uber.org/zap"
	"net/http"
)

type App struct {
	Srv        http.Server
	Router     *gin.Engine
	MemoryData DataPayload
}

type DataPayload struct {
	Data []string `json:"data"`
}

type health struct {
	Status string `json:"status"`
}

func (web *App) loggingMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ignorePaths := []string{"/healthcheck"}
		if slices.Contains(ignorePaths, c.Request.URL.Path) {
			return
		}
		start := time.Now()
		c.Next()
		latency := time.Since(start)
		log.Logger.Info("web response",
			zap.String("client_ip", c.ClientIP()),
			zap.String("method", c.Request.Method),
			zap.String("path", c.Request.URL.Path),
			zap.Int("status_code", c.Writer.Status()),
			zap.Int("body_size", c.Writer.Size()),
			zap.Duration("latency", latency))
	}
}

func (web *App) staticTypingMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Apply the Cache-Control header to the static files
		if strings.HasSuffix(c.Request.URL.Path, ".webmanifest") {
			c.Header("content-type", "application/manifest+json")
		}
		// Continue to the next middleware or handler
		c.Next()
	}
}

func (web *App) Initialize() {
	gin.SetMode(gin.ReleaseMode)
	web.Router = gin.New()
	web.Router.Use(otelgin.Middleware("gin"))
	web.Router.Use(web.loggingMiddleware())
	web.Router.Use(web.staticTypingMiddleware())
	web.initializeRoutes()
	for v := range 2500 {
		s := strconv.Itoa(v)
		web.MemoryData.Data = append(web.MemoryData.Data, s)

	}
	//web.MemoryData = DataPayload{
	//	Data: []string{"First", "Second"},
	//}
}

func (web *App) Run(addr string) {
	if addr == "" {
		addr = ":8080"
	}
	web.Srv = http.Server{
		Addr:    addr,
		Handler: web.Router,
	}
	err := web.Srv.ListenAndServe()
	if err != nil {
		log.Logger.Fatal("Issues with webserver: "+err.Error(),
			zap.Error(err))
	}
}

func (web *App) initializeRoutes() {
	web.Router.Use(static.Serve("/", static.LocalFile("dist", false)))
	web.Router.GET("/healthcheck", web.healthcheck)
	web.Router.GET("/api/v1/healthcheck", web.healthcheck)
	web.Router.GET("/api/v1/data", web.data)
	web.Router.GET("/api/v1/datagen", web.dataGen)
	web.Router.POST("/api/v1/data", web.updateData)
}

func (web *App) healthcheck(c *gin.Context) {
	data := health{
		Status: "UP",
	}
	c.JSON(http.StatusOK, data)
}

func (web *App) data(c *gin.Context) {
	c.JSON(http.StatusOK, web.MemoryData)
}

func (web *App) updateData(c *gin.Context) {
	//data, err := io.ReadAll(c.Request.Body)
	//if err != nil {
	//	log.Logger.Error("Unable to read body",
	//		zap.Error(err))
	//	c.JSON(http.StatusInternalServerError, "")
	//	return
	//}
	err := c.BindJSON(&web.MemoryData)
	if err != nil {
		log.Logger.Error("Unable to bind Json",
			zap.Error(err))
		c.JSON(http.StatusInternalServerError, web.MemoryData)
		return
	}
	c.JSON(http.StatusAccepted, web.MemoryData)
}

type randomData struct {
	Label1         string   `json:"label1"`
	Label2         string   `json:"label2"`
	Label3         string   `json:"label3"`
	Label4         string   `json:"label4"`
	Label5         string   `json:"label5"`
	Label6         string   `json:"label6"`
	Label7         string   `json:"label7"`
	Label8         string   `json:"label8"`
	Label9         string   `json:"label9"`
	Label10        string   `json:"label10"`
	MultipleLabels []string `json:"multiple_labels"`
}

func (web *App) dataGen(c *gin.Context) {
	var data []randomData
	for _ = range 2500 {
		local := randomData{
			Label1:  generateRandomString(10),
			Label2:  generateRandomString(10),
			Label3:  generateRandomString(10),
			Label4:  generateRandomString(10),
			Label5:  generateRandomString(10),
			Label6:  generateRandomString(10),
			Label7:  generateRandomString(10),
			Label8:  generateRandomString(10),
			Label9:  generateRandomString(10),
			Label10: generateRandomString(10),
			MultipleLabels: []string{
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
				generateRandomString(10),
			},
		}
		data = append(data, local)
	}
	c.JSON(http.StatusOK, data)
}

const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func generateRandomString(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}
