# PWA
```bash
nvm install --lts
nvm use --lts

npm create vite@latest pwa -- --template vue-ts

cd pwa

npm install -D tailwindcss@latest autoprefixer@latest
npx shadcn-vue@latest init

npm i vite-plugin-pwa -D
```

## Web Server
```bash
export OTEL_SERVICE_NAME="pwa"
export OTEL_EXPORTER_OTLP_ENDPOINT="https://otc-gateway.pineapplenest.com:443"
go run cmd/main.go
```